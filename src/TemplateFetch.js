/**
 * Fetches a resource from the server, e.g. pre-rendered template data or moustache templates
 *
 * @example
 * Initialize, recommended as global utility
 * window.tf = new TemplateFetch({
       host: window.location.protocol + '//' + window.location.host + '/',
       debug: true
   });

 * will execute in default the url protocol://host(:port)/api/templatefetch/<template-id>/?param=values&used_as=tpl_dat
 * tf.fetch('hello').then(function (response) {
       document.querySelector('body').insertAdjacentHTML('beforeend', response.html);
   });

 * Fetch and display the pre-rendered `view/templatefetch/overlay.twig`
 * tf.fetch('overlay', {
        css: 'example-overlay',
        btn: 'Close',
        content: '<p>Some Content!</p>',
    }).then(function (response) {
        document.querySelector('body').insertAdjacentHTML('beforeend', response.html);
    });

 * @version 0.2.0 - Demonstration Implementation
 */
class TemplateFetch {
    constructor(config) {
        this.config = Object.assign({
            // defaults.
            debug: false,
            host: '',
            api: {
                url: 'api/templatefetch/'
            }
        }, config);
    }

    /**
     * @todo add ie11 polyfill for fetching...
     *
     * @todo add cache
     * @param template_id
     * @param {Object} data used as get params
     */
    fetch(template_id, data = {}) {
        /**
         * Scope Helper
         * @type {TemplateFetch}
         */
        let self = this;
        return new Promise(function (resolve, reject) {
            try {
                let query = '';
                if (Object.keys(data).length >= 0 && data.constructor === Object) {
                    let esc = encodeURIComponent;
                    query = '?' + Object.keys(data)
                        .map(k => esc(k) + '=' + esc(data[k]))
                        .join('&');
                }
                fetch(self.config.host + self.config.api.url + template_id + query, {
                    method: 'GET',
                    cache: 'default'
                }).then(function (response) {
                    /*
                    bodyUsed: true
                    headers: Headers {  }
                    ok: true

                    redirected: false
                    status: 200
                    statusText: "OK"
                    type: "basic"
                    url: "https://host.tld/path?query=some"
                    */
                    if (200 === response.status) {
                        return response.json();
                    } else {
                        // todo: make granular
                        reject(Error('TemplateFetch got a not ok error code'));
                    }
                }).then(function (response) {
                    resolve(response);
                }).catch(function (err) {
                    reject(Error(err));
                })
            }
            catch (e) {
                reject(Error(e));
            }
        });
    }
}

export default TemplateFetch;