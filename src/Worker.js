/**
 * @todo: Just a draft, nothing to work with
 */

class Worker {
    /**
     *
     * @param {Object|function|string} callable
     * @returns {Worker}
     */
    constructor(callable) {
        /**
         *
         * @type {Worker|boolean}
         */
        this.worker = false;
        /**
         *
         * @type {Blob|BlobBuilder|boolean}
         */
        this.blob = false;

        this.debug = true;

        return this.init(callable);
    }

    /**
     *
     * @param callable
     * @returns {Worker}
     */
    init(callable) {
        if ('object' === typeof callable || 'function' === typeof callable || 'string' === typeof callable) {
            if ('object' === typeof callable || 'function' === typeof callable) {
                callable = callable.toString();
            } else if ('string' === typeof callable) {

            }

            try {
                this.blob = new Blob([callable], {type: 'application/javascript'});
            } catch (e) { // Backwards-compatibility
                window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder;
                this.blob = new BlobBuilder();
                this.blob.append(callable);
                this.blob = this.blob.getBlob();
            }
            return this;
        } else {
            if (this.debug) {
                console.error('CanalWorker: callable not a valid type.');
            }
        }
        return this;
    }

    run() {
        if (false !== this.blob) {
            console.log(this.blob);
            this.worker = new Worker(URL.createObjectURL(this.blob));
        } else {
            if (this.debug) {
                console.error('CanalWorker.run: this.blob is not valid.');
            }
        }

        return this;
    }

    postMessage(msg) {
        if (false !== this.worker) {
            this.worker.postMessage(msg);

            console.log(this.worker);
        } else {
            if (this.debug) {
                console.error('CanalWorker.postMessage: this.worker is not valid.');
            }
        }
        return this;
    }

    onMessage(callable) {
        if (false !== this.worker) {
            // Test, used in all examples:
            this.worker.onmessage = callable;
        } else {
            if (this.debug) {
                console.error('CanalWorker.onMessage: this.worker is not valid.');
            }
        }
        return this;
    }
}

export default Worker;