# Flood\Canal: Asset

JS + Sass for a [Flood\Canal](https://canal.bemit.codes) project.

See [documentation of Canal\Asset](https://bemit.codes/canal-asset).
 
Or [legacy documentation of Canal\Asset](https://painttheweb.de/flood-component/canal-asset)

# Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more information on the licences which are applied read: [LICENCE.md](LICENCE.md)

# Copyright

2017-2019 | [bemit UG (haftungsbeschränkt)](https://bemit.eu) - project@bemit.codes

Maintainer: [Michael Becker](https://mlbr.xyz)