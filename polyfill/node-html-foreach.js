// Polyfill for using forEach on a result of `querySelectorAll`
if ('function' !== typeof NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}
if ('function' !== typeof HTMLCollection.prototype.forEach) {
    HTMLCollection.prototype.forEach = Array.prototype.forEach;
}